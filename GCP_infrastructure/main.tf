provider "google" {
	credentials = file("account.json")
	project      = var.project
	region      = "europe-west1"
	zone      = "europe-west1-b"
}

