resource "google_project_service" "iam" {
	service = "iam.googleapis.com"

	disable_dependent_services = true
	disable_on_destroy = false
}


resource "google_service_account" "service_account" {
	account_id   = "sa-for-bucket"
	display_name = "Service account for bucket"
}

output "email" {
	value       = google_service_account.service_account.email
}

resource "google_service_account_key" "service_account_key" {
	service_account_id   = google_service_account.service_account.name
}

output "credentials" {
	value       = base64decode(google_service_account_key.service_account_key.private_key)
}


resource "google_storage_bucket_iam_binding" "editor" {
	bucket = google_storage_bucket.bucket.name
	role = "roles/storage.admin"
	members = [
		"serviceAccount:${google_service_account.service_account.email}",
	]
}



resource "local_file" "foo" {
	content = base64decode(google_service_account_key.service_account_key.private_key)
	filename = "${path.module}/service_account_for_bucket.json"
}

