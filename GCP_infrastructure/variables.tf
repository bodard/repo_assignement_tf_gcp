variable "project" {
	type = string
	description = "Name of the project in GCP"
	default = "worksample-vanessa"
}
