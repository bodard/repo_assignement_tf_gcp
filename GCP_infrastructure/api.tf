resource "google_project_service" "project" {
	service = "cloudresourcemanager.googleapis.com"

	disable_dependent_services = true
	disable_on_destroy = false
}

