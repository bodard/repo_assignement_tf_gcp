resource "google_storage_bucket" "bucket" {
	name       = "my_bucket_5475911e-591e-11ea-8e2d-0242ac130003"
	location   = "EU"
}

output "bucket" {
	value       = google_storage_bucket.bucket.name
}
