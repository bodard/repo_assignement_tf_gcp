
resource "google_project_service" "container" {
	service = "container.googleapis.com"

	disable_dependent_services = true
	disable_on_destroy = false
}

resource "google_container_cluster" "primary" {
	name       = "mycluster"
	initial_node_count = 3

	remove_default_node_pool = false
}


