provider "helm" {
	#kubernetes {
	#	host = "https://${google_container_cluster.primary.endpoint}"
	#	cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
	#	token = data.google_client_config.current.access_token
	#}
}

data "helm_repository" "bitnami" {
	name = "bitnami"
	url  = "https://charts.bitnami.com/bitnami/"
}

resource "helm_release" "etcd" {
	name       = "etcdrelease"
	repository = data.helm_repository.bitnami.metadata[0].name
	chart      = "etcd"
	#namespace = kubernetes_namespace.namespace.metadata.0.name
	
	#depends_on = [kubernetes_namespace.namespace]

}
