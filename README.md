
The credentials file giving access to Terraform should be in ./account.json

For sake of simplicity, the user should have full permissions over the project :
```
projectID="worksample-vanessa"
name_key_project="account.json"
service_account_name="projectowner"
service_account_projet=${service_account_name}@${projectID}.iam.gserviceaccount.com

gcloud iam service-accounts create ${service_account_name} --display-name ${service_account_name} --project ${projectID}  
gcloud projects add-iam-policy-binding ${projectID}   --member serviceAccount:${service_account_projet} --role roles/owner
gcloud iam service-accounts keys create ${name_key_project}     --iam-account ${service_account_projet} --project ${projectID}
```

The deployment is done in differents parts :

- The GCP ressources via TF : GCP_infrastructure/ contains all the TF files to provision all the GCP ressources
- The deployment on K8S : K8S_ressources/ contains TF files for helm and yaml for K8S 


# GCP resources :

```
cd ./GCP_infrastructure
terraform plan
terraform apply
```
It is possible that the deployment will fail due to the APIs not being ready yet. Retry 5 minutes later if this is the case.



# K8S resources :
Get the kubeconfig for the GKE ; it will be used by helm provider
```
gcloud container clusters get-credentials mycluster --zone europe-west1-b
```

Apply TF for the deploying etcd
```
cd ./K8S_resources
terraform plan
terraform apply
```

Create the secret containing the service-account creds for the bucket (which was produced by Terraform in the GCP ressources step) and apply the deployment.
```
kubectl create secret generic credentials --from-file=../GCP_infrastructure/service_account_for_bucket.json
kubectl apply -f deploy_voi.yaml
```



